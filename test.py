import treepoem
import os
os.environ["PATH"] += "C:\Program Files\gs\gs9.53.3\bin"
import os
from lxml import etree
from shutil import copyfile
import zipfile
image = treepoem.generate_barcode(
     barcode_type='gs1-128',  # One of the BWIPP supported codes.
     data='(01)13258172937301(21)BV50000292010000(240)02161236')
image.convert('1').save('barcode.png')