import os
import re
from openpyxl import load_workbook
from argparse import ArgumentParser
from barcode import ISSN
from barcode.writer import ImageWriter
from pylibdmtx.pylibdmtx import encode
import treepoem
from PIL import Image

parser = ArgumentParser()
parser.add_argument("-l", "--log",
                    default="example.log",
                    help="Input log file")

parser.add_argument("-t", "--template", default="template.xlsx",
                    help="XML template file")
args = parser.parse_args()

tester = ""
tester_id = 0
data = []
#Parsing log
log = open(args.log,'r')
for line in log.readlines():
    if ("Data" in line):
        tester = re.findall("Tester=(.+?),",line)[0]
        tester_id = re.findall("TesterID=(.+?)\n", line)[0]
        continue
    if "SUCCESS" not in line:
        continue
    data.append({
        'RSKU': re.findall("RSKU=(.+?),", line)[0],
        'TDS': re.findall("TDS=(.+?),", line)[0],
        'Release': re.findall("Release=(.+?),", line)[0],

        'Cases': re.findall("\((.+?)->", line)[0],
        'Outers': re.findall("->(.+?)->", line)[0],
        'Packs': re.findall("->([0-9]*?)\)", line)[0],
    })

#Making copy of template

#Getting list of SKUs
wb = load_workbook(args.template)
skus_wb = wb.get_sheet_by_name("SKUs")
skus = [skus_wb["A"+str(i)].value for i in range(2,len(skus_wb['A']))]

#Filling cells
if (not os.path.exists("./output/")):
    os.mkdir("./output/")

for i in data:
    #v16_<rsku>_R<Release>_TDS<TestDataSet>-<number of cases>-<number of outers>-<number of packs>
    if (i['RSKU'] in skus):
        dirname = "v16_{}_R{}_TDS{}".format(i['RSKU'], i['Release'], i['TDS'])
        if (not os.path.exists("./output/"+dirname)):
            os.mkdir("./output/"+dirname)
            os.mkdir("./output/" + dirname+"/codes")
            os.mkdir("./output/" + dirname + "/datamatrixes")
        print(i)

        name = "v16_{}_R{}_TDS{}_{}_{}_{}.xlsx".format(i['RSKU'], i['Release'], i['TDS'], i['Cases'], i['Outers'], i['Packs'])
        wb = load_workbook(args.template, data_only=True)
        params = wb.get_sheet_by_name("Parameters")
        params["C7"]  = str(i['RSKU']).zfill(8)
        params["C38"] = str(tester)
        params["C39"] = int(tester_id)
        params["C40"] = int(i['Release'])
        params["C42"] = int(i['TDS'])
        for iterator in range(20):
            barname = str(tester)+i['Release']+str(params["C46"].value)+str(params["C41"].value)+str(params["C42"].value)+str(10*iterator+2010)+"0000"
            barcode = "(01){}(21){}(240){}".format(str(params["C21"].value),barname,str(params["C7"].value))

            encoded = encode(barname)
            img = Image.frombytes('RGB', (encoded.width, encoded.height), encoded.pixels)
            img.save("./output/" + dirname +"/datamatrixes/{}.png".format(barname))

            image = treepoem.generate_barcode(
                barcode_type='gs1-128',  # One of the BWIPP supported codes.
                data=barcode)
            image.convert('1').save("./output/" + dirname +"/codes/{}.png".format(barname))
        wb.save("./output/" + dirname + "/" + name)
        print("Created document: {}".format(name))
    else:
        print("No SKU with id {}".format(i['RSKU']))

